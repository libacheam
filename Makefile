DESTDIR  ?= /usr/local
CFLAGS   := -Wall -Wextra -pedantic -std=c99 -fPIC ${CFLAGS}

all: shared static man pkgconfig

shared:
	${CC} -shared ${LDFLAGS} -o libacheam.so src/libacheam/*.c ${CFLAGS}

static:
	${CC} ${LDFLAGS} src/libacheam/*.c ${CFLAGS} -c
	ar rcs libacheam.a *.o

pkgconfig:
	sed "s|PREFIX_HERE|${DESTDIR}|g" acheam.pc.in > acheam.pc

man:
	for i in man/*.scd; do \
		outp=$$(echo "$$i" | rev | cut -f 2- -d '.' | rev); \
		scdoc < $$i > $$outp; \
		echo ".SH COLOPHON\n This page is a part of libacheam(3).\&" >> $$outp; \
	done

install: all
	mkdir -p ${DESTDIR}/include \
		${DESTDIR}/lib/pkgconfig \
		${DESTDIR}/man/man3

	cp libacheam.so           ${DESTDIR}/lib
	cp acheam.pc              ${DESTDIR}/lib/pkgconfig
	cp man/*.3                ${DESTDIR}/man/man3/
	cp src/libacheam/acheam.h ${DESTDIR}/include

uninstall:
	rm ${DESTDIR}/include/acheam.h \
		${DESTDIR}/lib/libacheam.so \
		${DESTDIR}/lib/pkgconfig/acheam.pc

	for i in man/*.scd; do \
		rm ${DESTDIR}/man/man3/$$(basename "$$i" ".scd"); \
	done

test: static
	${CC} -o test src/tests.c -I./src/libacheam libacheam.a ${CFLAGS}
	./test

clean:
	rm -f test libacheam.so libacheam.a *.o man/*.3

.POSIX:
.PHONY: all man test
