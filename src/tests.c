#include <acheam.h>
#include <assert.h>

#include <string.h>
#include <stdio.h>

int
main(void)
{
	// tolowerw()
	assert(strcmp(tolowerw("eXaMpLe StRiNg"), "example string") == 0);

	// toupperw()
	assert(strcmp(toupperw("eXaMpLe StRiNg"), "EXAMPLE STRING") == 0);

	printf("All tests passed succesfully!\n");
	return 0;
}
