#include <ctype.h>
#include <stdlib.h>
#include <string.h>

char *
tolowerw(char *inp)
{
	char *tmp = calloc(strlen(inp), sizeof(char));
	strcpy(tmp, inp);
	for (int i = 0; tmp[i]; i++){
		tmp[i] = tolower(tmp[i]);
	}
	return tmp;
}

